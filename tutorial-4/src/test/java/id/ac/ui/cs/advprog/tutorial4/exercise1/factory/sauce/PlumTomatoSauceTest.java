package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PlumTomatoSauceTest {

    private PlumTomatoSauce mySauce;

    @Before
    public void setUp() throws Exception {
        mySauce = new PlumTomatoSauce();
    }

    @Test
    public void testThisIsPlumTomatoSauce() {
        assertEquals("" + mySauce, "Tomato sauce with plum tomatoes");
    }
}
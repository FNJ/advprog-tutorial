package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory myFactory;

    @Before
    public void setUp() throws Exception {
        myFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testDepokFactoryCanCreateMatchCheese() {
        assertEquals((myFactory.createCheese() instanceof MozzarellaCheese), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchClams() {
        assertEquals((myFactory.createClam() instanceof FrozenClams), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchDough() {
        assertEquals((myFactory.createDough() instanceof ThickCrustDough), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchSauce() {
        assertEquals((myFactory.createSauce() instanceof PlumTomatoSauce), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchVeggies() {
        Veggies[] result = myFactory.createVeggies();
        assertEquals((result[0] instanceof BlackOlives), true);
        assertEquals((result[1] instanceof Eggplant), true);
        assertEquals((result[2] instanceof Spinach), true);
    }
}
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GarlicTest {

    private Garlic myVeggies;

    @Before
    public void setUp() throws Exception {
        myVeggies = new Garlic();
    }

    @Test
    public void testThisIsGarlic() {
        assertEquals("" + myVeggies, "Garlic");
    }
}
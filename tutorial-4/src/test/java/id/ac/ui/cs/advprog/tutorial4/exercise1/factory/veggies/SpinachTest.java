package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SpinachTest {

    private Spinach myVeggies;

    @Before
    public void setUp() throws Exception {
        myVeggies = new Spinach();
    }

    @Test
    public void testThisIsSpinach() {
        assertEquals("" + myVeggies, "Spinach");
    }
}
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory myFactory;

    @Before
    public void setUp() throws Exception {
        myFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testDepokFactoryCanCreateMatchCheese() {
        assertEquals((myFactory.createCheese() instanceof ReggianoCheese), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchClams() {
        assertEquals((myFactory.createClam() instanceof FreshClams), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchDough() {
        assertEquals((myFactory.createDough() instanceof ThinCrustDough), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchSauce() {
        assertEquals((myFactory.createSauce() instanceof MarinaraSauce), true);
    }

    @Test
    public void testDepokFactoryCanCreateMatchVeggies() {
        Veggies[] result = myFactory.createVeggies();
        Veggies[] tester = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
        assertEquals((result[0] instanceof Garlic), true);
        assertEquals((result[1] instanceof Onion), true);
        assertEquals((result[2] instanceof Mushroom), true);
        assertEquals((result[3] instanceof RedPepper), true);
    }
}
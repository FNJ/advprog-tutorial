package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {

    private MarinaraSauce mySauce;

    @Before
    public void setUp() throws Exception {
        mySauce = new MarinaraSauce();
    }

    @Test
    public void testThisIsMarinaraSauce() {
        assertEquals("" + mySauce, "Marinara Sauce");
    }
}
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MushroomTest {

    private Mushroom myVegies;

    @Before
    public void setUp() throws Exception {
        myVegies = new Mushroom();
    }

    @Test
    public void testThisIsMushrooms() {
        assertEquals("" + myVegies, "Mushrooms");
    }
}
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FreshClamsTest {

    private FreshClams myClam;

    @Before
    public void setUp() throws Exception {
        myClam = new FreshClams();
    }

    @Test
    public void testThisIsFreshClams() {
        assertEquals("" + myClam, "Fresh Clams from Long Island Sound");
    }
}
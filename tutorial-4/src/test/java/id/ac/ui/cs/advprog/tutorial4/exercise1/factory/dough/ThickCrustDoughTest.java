package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {

    private ThickCrustDough myDough;

    @Before
    public void setUp() throws Exception {
        myDough = new ThickCrustDough();
    }

    @Test
    public void testThisIsThickCrustDough() {
        assertEquals("" + myDough, "ThickCrust style extra thick crust dough");
    }
}
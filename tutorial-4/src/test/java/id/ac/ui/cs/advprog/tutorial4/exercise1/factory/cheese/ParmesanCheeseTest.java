package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ParmesanCheeseTest {

    private ParmesanCheese myCheese;

    @Before
    public void setUp() throws Exception {
        myCheese = new ParmesanCheese();
    }

    @Test
    public void testThisIsParmesanCheese() {
        assertEquals("" + myCheese, "Shredded Parmesan");
    }
}
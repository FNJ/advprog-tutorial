package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BlackOlivesTest {

    private BlackOlives myVeggies;

    @Before
    public void setUp() throws Exception {
        myVeggies = new BlackOlives();
    }

    @Test
    public void testThisIsBlackOlive() {
        assertEquals("" + myVeggies, "Black Olives");
    }
}
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {

    private FrozenClams myClam;

    @Before
    public void setUp() throws Exception {
        myClam = new FrozenClams();
    }

    @Test
    public void testThisIsFrozenClams() {
        assertEquals("" + myClam, "Frozen Clams from Chesapeake Bay");
    }
}
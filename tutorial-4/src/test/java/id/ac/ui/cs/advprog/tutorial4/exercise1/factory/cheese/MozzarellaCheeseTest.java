package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {

    private MozzarellaCheese myCheese;

    @Before
    public void setUp() throws Exception {
        myCheese = new MozzarellaCheese();
    }

    @Test
    public void testThisIsMozzarellaCheese() {
        assertEquals("" + myCheese, "Shredded Mozzarella");
    }
}
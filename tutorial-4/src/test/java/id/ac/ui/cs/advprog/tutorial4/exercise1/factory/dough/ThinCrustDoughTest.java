package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinCrustDoughTest {

    private ThinCrustDough myDough;

    @Before
    public void setUp() throws Exception {
        myDough = new ThinCrustDough();
    }

    @Test
    public void testThisIsThinCrustDough() {
        assertEquals("" + myDough, "Thin Crust Dough");
    }
}
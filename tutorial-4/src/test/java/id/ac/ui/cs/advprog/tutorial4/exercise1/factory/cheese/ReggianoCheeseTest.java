package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {

    private ReggianoCheese myCheese;

    @Before
    public void setUp() throws Exception {
        myCheese = new ReggianoCheese();
    }

    @Test
    public void testThisIsReggianoCheese() {
        assertEquals("" + myCheese, "Shredded Reggiano");
    }
}
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RedPepperTest {

    private RedPepper myVeggies;

    @Before
    public void setUp() throws Exception {
        myVeggies = new RedPepper();
    }

    @Test
    public void testThisIsRedPepper() {
        assertEquals("" + myVeggies, "Red Pepper");
    }
}
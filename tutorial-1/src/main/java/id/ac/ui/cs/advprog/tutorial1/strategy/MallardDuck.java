package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
	// TODO Complete me!
	public MallardDuck(){
		super();
		setQuackBehavior(new Quack());
		setFlyBehavior(new FlyWithWings());
	}

	public void display(){
		System.out.println("I'm a real Mallard duck");
	}
}

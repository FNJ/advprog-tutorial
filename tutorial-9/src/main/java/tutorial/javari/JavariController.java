package tutorial.javari;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.ArrayList;
import java.util.Scanner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

public class JavariController {
    // TODO Implement me!

    private final AtomicLong counter = new AtomicLong();
    private ArrayList<Animal> myAnimal = new ArrayList<Animal>();

    @RequestMapping(method=RequestMethod.GET, value = "/javari", produces="application/json")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public String getJavari() {
        String fileName = "myAnimal.csv";
        String retVal = "{'animal':[";
        File file = new File(fileName);
        try{
            Scanner inStream = new Scanner(file);
            inStream.useDelimiter("[,\n]");
            if (!inStream.hasNext()) {
                return "Warning{" +
                        "value='Javari don't have animal'}";
            }
            while (inStream.hasNext()){
                String id = inStream.next();
                String type = inStream.next();
                String name = inStream.next();
                String gender = inStream.next();
                String length = inStream.next();
                String weight = inStream.next();
                String condition = inStream.next();

                retVal += "{id="+
                        id + " type='" +
                        type + "' name='" +
                        name + "' gender='" +
                        gender + "' length=" +
                        length + "weight=" +
                        weight + "condition='" +
                        condition + "'},";
            }
            retVal = retVal.substring(0, -1) + "]}";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/javari/{id}", produces="application/json")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public String getOneJavari(@PathVariable("id") int id) {
        String fileName = "myAnimal.csv";
        File file = new File(fileName);
        String retVal = "";
        try{
            Scanner inStream = new Scanner(file);
            inStream.useDelimiter("[,\n]");
            if (!inStream.hasNext()) {
                return "{Warning:{" +
                        "value='Javari don't have animal'}}";
            }
            while (inStream.hasNext()){
                String idData = inStream.next();
                String type = inStream.next();
                String name = inStream.next();
                String gender = inStream.next();
                String length = inStream.next();
                String weight = inStream.next();
                String condition = inStream.next();

                if (id == Integer.parseInt(idData)) {
                    retVal += "{id="+
                            idData + " type='" +
                            type + "' name='" +
                            name + "' gender='" +
                            gender + "' length=" +
                            length + "weight=" +
                            weight + "condition='" +
                            condition + "'},";
                    break;
                }
            }
            if (retVal == "") {
                return "{Warning:{" +
                        "value='" + id + "is not Javari's animal id'}}";
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return retVal;
    };

    @RequestMapping(method=RequestMethod.POST, value = "/javari", produces="application/json")
    @ResponseBody
    public String newAnimalIn ( 
            @RequestParam("type") String type,
            @RequestParam("name") String name,
            @RequestParam("gender") String gender,
            @RequestParam("length") double length,
            @RequestParam("weight") double weight,
            @RequestParam("condition") String condition) {
        Gender thisGen = Gender.FEMALE;
        Condition thisCon = Condition.HEALTHY;
        if (gender.equalsIgnoreCase("male")) {
            thisGen = Gender.MALE;
        }
        if (condition.equalsIgnoreCase("sick")) {
            thisCon = Condition.SICK;
        }
        Animal hehe = new Animal(Integer.parseInt(counter.toString()),
                type, name, thisGen, length, weight, thisCon);
        myAnimal.add(hehe);
        
        return "";
    }

    @RequestMapping(method=RequestMethod.DELETE, value = "/javari/{id}", produces="application/json")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public String deleteOneJavari(@PathVariable("id") int id) {
        return "";
    }
}

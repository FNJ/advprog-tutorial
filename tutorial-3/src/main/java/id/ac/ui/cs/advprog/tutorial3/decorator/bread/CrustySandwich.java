package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class CrustySandwich extends Food {
    double myCost;

    public CrustySandwich() {
        //TODO Implement
        myCost = 1.0;
    }

    @Override
    public double cost() {
        //TODO Implement
        return myCost;
    }

    public String getDescription() {
        return "Crusty Sandwich";
    }
}

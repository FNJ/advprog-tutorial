package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        //TODO Implement
        if (salary < 30000) {
            throw new IllegalArgumentException("Salary < 30000.00 ");
        }
        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}

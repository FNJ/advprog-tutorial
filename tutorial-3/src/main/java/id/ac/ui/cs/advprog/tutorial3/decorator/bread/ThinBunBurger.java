package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThinBunBurger extends Food {
    double myCost;

    public ThinBunBurger() {
        //TODO Implement
        myCost = 1.5;
    }

    @Override
    public double cost() {
        //TODO Implement
        return myCost;
    }

    public String getDescription() {
        return "Thin Bun Burger";
    }
}

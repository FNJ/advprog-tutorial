import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class PrimeCheckerTest {

    private static final List<Integer> PRIME_NUMBERS = Arrays.asList(2, 3, 5, 7);

    @Test
    public void testIsPrimeTrueGivenPrimeNumbers() {
        PRIME_NUMBERS.forEach(number -> assertTrue(PrimeChecker.isPrime(number)));
    }

    @Test
    public void testIsPrimeFalseGivenNonPrimeNumbers() {
        try {
            assertFalse(PrimeChecker.isPrime(9));
        } catch (Exception e) {
            fail("TODO Implement me!");
        }
        // Given non-prime numbers
        // When isPrime is invoked
        // It should return false
    }

    @Test
    public void testMainAndClass() {
        PrimeChecker newPc = new PrimeChecker();
        PrimeChecker.main(null);
    }
}
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    private Map<String, Integer> scores;
    private Map<Integer, List<String>> expect;
    private List<String> isi;

    @Before
    public void setUp() throws Exception {
        scores = new HashMap<>();
        expect = new HashMap<>();
        isi = Arrays.asList("Alice", "Bob", "Charlie");

        scores.put("Alice", 12);
        scores.put("Bob", 12);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Charlie", 12);

        expect.put(12, isi);
        isi = Arrays.asList("Delta", "Emi");
        expect.put(15, isi);
    }

    @Test
    public void scoreShouldGroupedWell() {
        Map<Integer, List<String>> hasil = ScoreGrouping.groupByScores(scores);
        assertEquals("{12=[Bob, Alice, Charlie], 15=[Emi, Delta]}", hasil.toString());
    }

    @Test
    public void testMainAndClassSgt() {
        ScoreGrouping newSgt = new ScoreGrouping();
        ScoreGrouping.main(null);
    }
}
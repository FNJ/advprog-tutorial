package tallycounter;

//import java.util.concurrent.atomic.AtomicInteger;

public class TallyCounter  {
    private int counter = 0;

//    ------------- initial tally counter ----------------------
//    public void increment() {
//        counter++;
//    }
//
//    public void decrement() {
//        counter--;
//    }
//
//    public int value() {
//        return counter;
//    }

//  --------------- AtomicInteger TallyCounter ----------------
//  private AtomicInteger counter= new AtomicInteger();

//  public void increment() {
//      counter.incrementAndGet();
//  }
//
//  public void decrement() {
//      counter.decrementAndGet();
//  }
//
//  public int value() {
//      return counter.get();
//  }
// -----------------Synchronized TallyCounter-------------------
    public synchronized void increment() {
        counter++;
    }

    public synchronized void decrement() {
        counter--;
    }

    public synchronized int value() {
        return counter;
    }
}

package game;

/**
 * {@inheritDoc}
 * email : franna.jaya@gmail.com.
 * Student at Universitas Indonesia.
 * @author Fran Na Jaya
 */
public class PointCheckerPerSec implements Runnable {

    private PointCounter pointCounterReference;
    private Thread thread;
    private int sleepTime;
    private boolean state;

    public PointCheckerPerSec(PointCounter pointCounterReference) {
        this.pointCounterReference = pointCounterReference;
        this.sleepTime = 1000;
        this.state = true;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this, "Point Counter Checker");
            thread.start();
        }
    }

    public void changeState() {
        state = !state;
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {
            do {
                Thread.sleep(sleepTime);
                pointCounterReference.decrement();
            } while (state);
        } catch (InterruptedException e) {
            System.out.println("Point Counter Checker interrupted.");
        }
        System.out.println("");
    }

}

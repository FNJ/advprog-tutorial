package game;

/**
 * {@inheritDoc}
 * email : franna.jaya@gmail.com.
 * Student at Universitas Indonesia.
 * @author Fran Na Jaya.
 */
public class PointCounter {
    private double counter = 100;

    public synchronized void increment() {
        counter++;
    }

    public synchronized void decrement() {
        counter--;
    }

    public synchronized double value() {
        return counter;
    }

    /**
     * {@inheritDoc}
     * this is method to increment score if the answer is true and below threshold.
     */
    public synchronized double incrementBelowThreshold() {
        // TODO Auto-generated method stub
        double ret = counter * 0.1;
        counter += ret;
        return ret;
    }

    /**
     * {@inheritDoc}
     * this is method to increment score if the answer is true and above threshold.
     * 
     */
    public synchronized double incrementAboveThreshold() {
        // TODO Auto-generated method stub
        double ret = counter * 0.05;
        counter += ret;
        return ret;
    }
}

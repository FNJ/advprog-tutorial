package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import id.ac.ui.cs.advprog.tutorial2.exercise1.receiver.CeilingFan;

public abstract class CeilingFanCommand implements Command {

    protected CeilingFan ceilingFan;
    protected int prevSpeed;

    public CeilingFanCommand(CeilingFan ceilingFan) {
        this.ceilingFan = ceilingFan;
    }

    @Override
    public void execute() {
        // TODO Complete me!
        prevSpeed = ceilingFan.getSpeed();
        operate();
    }

    protected abstract void operate();

    @Override
    public void undo() {
        if (prevSpeed == ceilingFan.HIGH) {
            // TODO Complete me!
            ceilingFan.high();
        } else if (prevSpeed == ceilingFan.MEDIUM) {
            ceilingFan.medium();
        } else if (prevSpeed == ceilingFan.LOW) {
            ceilingFan.low();
        } else if (prevSpeed == ceilingFan.OFF) {
            // TODO Complete me!
            ceilingFan.off();
        }
    }
}

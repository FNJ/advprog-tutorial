package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Stream;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        commands.stream().forEach(this::exe);
    }

    @Override
    public void undo() {
        // TODO Complete me!
        for(int x = commands.size()-1; x >= 0; x--){
            commands.get(x).undo();
        }
    }

    private void exe(Command c) {
        //body of the for loop
        c.execute();
    }
}
